function Validator(formSelector) {
  var _this = this;
  var formRules = {};
  function getParent(element, selector) {
    while (element.parentElement) {
      if (element.parentElement.matches(selector)) {
        return element.parentElement;
      } else {
        element = element.parentElement;
      }
    }
  }
  // Convention:
  // if have errors so return `errors message`
  // if haven't errors so return undefined
  var validatorRules = {
    required: function (value) {
      return value ? undefined : 'Vui lòng nhập trường này';
    },
    email: function (value) {
      var regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
      return regex.test(value)
        ? undefined
        : 'Vui lòng nhập đúng định dạng email';
    },
    min: function (min) {
      return function (value) {
        return value.length >= min
          ? undefined
          : `Vui lòng nhập mật khẩu tối thiểu ${min} kí tự`;
      };
    },
  };
  //get form element in DOM follow formSelector
  var formElement = document.querySelector(formSelector);
  //just handle when have formElement
  if (formElement) {
    var inputs = formElement.querySelectorAll('[name][rules]');
    for (var input of inputs) {
      //console.log(input);
      var rules = input.getAttribute('rules').split('|'); //split cut string become array["required","email"]
      // console.log(rules);
      for (var rule of rules) {
        var ruleInfo;
        var isRuleHasValue = rule.includes(':');
        if (isRuleHasValue) {
          ruleInfo = rule.split(':');
          //console.log(ruleInfor);
          rule = ruleInfo[0];
          // console.log(rule);
          // console.log(ruleInfo[1]);
        }
        var ruleFunc = validatorRules[rule]; // validatorRules[rule] is a function so nested function inside
        //console.log(typeof ruleFunc);//function
        if (isRuleHasValue) {
          //ruleFunc = ruleFunc(ruleInfo[1]);
          ruleFunc = ruleFunc(ruleInfo[1]);
          //console.log('Nhay vao if');
        }

        //console.log(rule)
        // console.log(formRules[input.name]);
        if (Array.isArray(formRules[input.name])) {
          //formRules[input.name] = {key:value} //var inputs = formElement.querySelectorAll('[name][rules]');
          formRules[input.name].push(ruleFunc);
        } else {
          formRules[input.name] = [ruleFunc]; // just in case one time, so assign first rule(required)
        }
      }
      //listen event for validate
      input.onblur = handleValidate;
      input.onchange = handleClearMessage;
    }
    function handleValidate(event) {
      var rules = formRules[event.target.name]; //through name check rules
      //console.log(rules);
      var errorMessage;
      for (var rule of rules) {
        errorMessage = rule(event.target.value);
        if (errorMessage) break;
      }
      // rules.find(function (rule) {
      //   // function find() return a function => rule()
      //   // transmission value in function
      //   errorMessage = rule(event.target.value); //value of rule trans for errorMessage
      //   return errorMessage;})
      //console.log(typeof errorMessage);
      //if have errors then show error message in UI
      if (errorMessage) {
        var formGroup = getParent(event.target, '.form-group');
        if (formGroup) {
          formGroup.classList.add('invalid');
          var formMessage = formGroup.querySelector('.form-message');
          if (formMessage) {
            formMessage.innerText = errorMessage;
            console.log(formMessage);
          }
        }
      }
      return !errorMessage; //have error = true, ! = false
    }
    function handleClearMessage(event) {
      var formGroup = getParent(event.target, '.form-group');
      if (formGroup.classList.contains('invalid')) {
        formGroup.classList.remove('invalid');
        var formMessage = formGroup.querySelector('.form-message');
        if (formMessage) {
          formMessage.innerText = '';
        }
      }
    }
    //console.log(formRules);
  }
  //console.log(this);
  formElement.onsubmit = function (event) {
    event.preventDefault();
    var inputs = formElement.querySelectorAll('[name][rules]');
    var isValid = true;
    for (var input of inputs) {
      //!! errorMessage
      //console.log(handleValidate({ target: input })); //true
      if (!handleValidate({ target: input })) {
        //event.target.input && ({target: input} = X) => X.target = event.target = target.input get name
        isValid = false;
      }
    }
    if (isValid) {
      if (typeof _this.onSubmit === 'function') {
        var enableInput = formElement.querySelectorAll('[name]');
        var formValues = Array.from(enableInput).reduce(function (
          values,
          input
        ) {
          switch (input.type) {
            case 'radio':
              // if (input.matches(':checked')) {
              //   values[input.name] = input.value;
              // } else {
              //   values[input.name] = '';
              // }
              values[input.name] = formElement.querySelector(
                'input[name = "' + input.name + '"]:checked'
              ).value;
              break;
            case 'checkbox':
              // Không checked thì cho value là một mảng
              if (!values[input.name]) values[input.name] = [];
              // Nếu checked thì push vào mảng
              if (input.checked) values[input.name].push(input.value);
              // Kiểm tra nếu là mảng rỗng thì gán là chuỗi ''
              if (values[input.name].length === 0) values[input.name] = '';
              break;
              // if (!input.matches(':checked')) {
              //   values[input.name] = '';
              //   return values;
              // }
              // if (!Array.isArray(values[input.name])) {
              //   values[input.name] = [];
              // }
              // values[input.name].push(input.value);
              //   if (!input.matches(':checked')) {
              //     values[input.name] = "";
              //     return values;
              //   }
              //   if (!Array.isArray(values[input.name])) {
              //     values[input.name] = [];
              //   }
              //   values[input.name].push(input.value);
              //   break;
              // case 'file':
              //   values[input.name] = input.files;
              break;
            default:
              values[input.name] = input.value;
          }
          return values;
        },
        {});
        _this.onSubmit(formValues);
      } else {
        formElement.submit();
      }
    }
  };
}
